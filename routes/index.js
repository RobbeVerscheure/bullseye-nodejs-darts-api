let express = require('express');
let router = express.Router();
let db = require('../database/db');
let fw = require('../files/fileWorker');
let cors = require('cors');


router.get('/api/players', cors(), (req, res) => {
    db.getAllPlayers(result => {
        res.json(result);
    })
});

router.get('/api/player/:id', cors(), (req,res) => {
    let id = req.params.id;
    db.getPlayerById(id, result => {
        res.send(result);
    })
});

router.get('/api/tournaments', cors(), (req,res) => {
    db.getAllTournaments(result => {
        res.send(result);
    })
});

router.get('/api/tournament/:id', cors(),(req,res) => {
    let id= req.params.id;
    db.getTournamentById(id,result => {
        res.json(result)
    } )
});


router.get('/api/tournament/:id/matches', cors(), (req,res) => {
    let id = req.params.id;
    db.getDetailedMatchesFromTournamentId(id, result =>{
        res.json(result);
    })
});

router.get('/api/matches/:id', cors(), (req,res) => {
    let id = req.params.id;
    db.getLast3MatchesFromPlayerId(id, result =>{
        res.json(result);
    })
});


router.get('/api/article',cors(), (req,res) => {
    fw.getAllArticles(result => {
        res.json(JSON.parse(result));
    }) ;
});



module.exports = router;
