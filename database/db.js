let mysql = require("mysql");
let config = {
   "INSERT_DB_CONFIG"
};


let connection = mysql.createConnection(config);

connection.connect(function (err) {
    if (err) throw err;
    console.log("Connected");
});

//Player queries
let getPlayersQuery = "select * from player";
let getPlayerByIdQuery = "select * from player where id =?";

//Tournaments
let getTournamentsQuery = "select * from tournament";
let getTournamentByIdQuery = "select * from tournament where id=?";

//Matches
let getLast3MatchesFromPlayerIdQuery = "SELECT * FROM ID314755_bullseye.match WHERE (first_player = 1 OR second_player = 1) AND (winner != 0) ORDER BY DATE DESC LIMIT 3"
let getDetailedMatchesFromTournamentIdQuery =   "SELECT M.id, M.date, P1.first_name AS p1_first_name,P1.last_name AS p1_last_name, P1.picture AS p1_picture,P2.first_name AS p2_first_name,P2.last_name AS p2_last_name, P2.picture AS p2_picture FROM ID314755_bullseye.match AS M \n" +
    "INNER JOIN\n" +
    " player AS P1 ON P1.id = M.first_player\n" +
    " INNER JOIN\n" +
    " player AS P2 ON P2.id = M.second_player\n" +
    " WHERE tournament_id=? ";


module.exports = {
    getAllPlayers,
    getPlayerById,
    getAllTournaments,
    getTournamentById,
    getDetailedMatchesFromTournamentId,
    getLast3MatchesFromPlayerId
};

function getAllPlayers(cb) {
    let players = [];
    connection.query(getPlayersQuery, (err, rows) => {
        if (err) throw err;
        rows.forEach(row => {
            players.push(row);
        });
        cb(players)
    })
}

function getPlayerById(id,cb) {

    let player = [];
    connection.query(getPlayerByIdQuery, [parseInt(id)], (err, rows) => {
        if (err) throw err;
        rows.forEach(row => {
            player.push(row);
        });
        cb(player);
    })

}

function getAllTournaments(cb) {
    let tournaments = [];
    connection.query(getTournamentsQuery, (err, rows) => {
        if (err) throw err;
        rows.forEach(row => {
            tournaments.push(row);
        });
        cb(tournaments);
    })
}

function getTournamentById(id,cb){
    let tournaments = [];
    connection.query(getTournamentByIdQuery, [parseInt(id)],(err, rows) => {
        if (err) throw err;
        rows.forEach(row => {
            tournaments.push(row);
        });
        cb(tournaments);
    })
}

function getDetailedMatchesFromTournamentId(id,cb){
    let tournaments = [];
    connection.query(getDetailedMatchesFromTournamentIdQuery, [parseInt(id)], (err, rows) => {
        if (err) throw err;
        rows.forEach(row => {
            tournaments.push(row);
        });
        cb(tournaments)
    })
}

function getLast3MatchesFromPlayerId(id, cb) {
    let matches = [];
    connection.query(getLast3MatchesFromPlayerIdQuery, [parseInt(id),parseInt(id)], (err, rows) => {
        if (err) throw err;
        rows.forEach(row => {
            matches.push(row);
        });
        cb(matches)
    })
}
