let fs = require('fs');

module.exports = {
    getAllArticles
};

function getAllArticles(cb) {

    fs.readFile('files/article.json', "utf-8", (err, data) => {
        if (err) { console.log(err) }
      cb(data);
    });

}